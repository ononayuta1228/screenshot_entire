import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:screenshot/screenshot.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

void main() => runApp(MyApp());

Future<void> writeToFile(ByteData data, String path) {
  final buffer = data.buffer;
  return File(path)
      .writeAsBytes(buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
}

void capture({@required String path}) async {
  var builder = ui.SceneBuilder();
  var scene = RendererBinding.instance.renderView.layer.buildScene(builder);
  var image = await scene.toImage(ui.window.physicalSize.width.toInt(),
      ui.window.physicalSize.height.toInt());
  scene.dispose();

  var data = await image.toByteData(format: ui.ImageByteFormat.png);
  await writeToFile(data, path);
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  File _imageFile;

  //Create an instance of ScreenshotController
  ScreenshotController screenshotController = ScreenshotController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Draw test'),
      ),
      body: Column( //ここを削除するとなぜかscreenshot範囲が画面内になる。でも表示崩れは治る
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Screenshot(
            controller: screenshotController,
            child: Container(
              color: Colors.white,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 200,
                      color: Colors.green,
                    ),
                    Container(
                      height: 200,
                      color: Colors.blue,
                    ),
                    Container(
                      height: 200,
                      color: Colors.red,
                    ),
                    Container(
                      height: 200,
                      color: Colors.yellow,
                    ),
                    Container(
                      height: 200,
                      color: Colors.brown,
                    ),
                    Container(
                      height: 200,
                      color: Colors.orange,
                      child: RaisedButton(
                        onPressed: () {
                          // ここは自分の保存したい領域を絶対パスで記載する
                          capture(path: '/Users/ononayuta/git/screen_shot_entire/image1.png');
                        },
                        child: Text('Hello world'),
                      ),
                    ),
                    _imageFile != null ? Image.file(_imageFile) : Container(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _imageFile = null;
          screenshotController
              .capture(path: '/Users/ononayuta/git/screen_shot_entire/image2.png', //ここも絶対パスで記載
              delay: Duration(milliseconds: 10))
              .then((File image) async {
            //print("Capture Done");
            setState(() {
              _imageFile = image;
            });
            final result =
            await ImageGallerySaver.saveImage(image.readAsBytesSync());
            print("File Saved to Gallery");
          }).catchError((onError) {
            print(onError);
          });
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}